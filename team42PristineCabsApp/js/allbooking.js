// this page show the script for the all booking page
"use strict";

// show all booking list to the allBooking.html
// from local storage queue
// return void
function bookingList() {
    let queue = bookingSession._queue
    let bookingContent = '';
    for (let bookingIndex = 0; bookingIndex < booking.length; bookingIndex++) {
        // show booking number
        bookingContent += '<div class="demo-updates mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--12-col-desktop">';
        bookingContent += '<div class="mdl-card__title mdl-list__item">';
        bookingContent += '<h4 class="mdl-card__title-text mdl-list__item-primary-content">' + bookingList[bookingIndex].bookingNumber + '</h4>';
        // show info icon
        bookingContent += '<span class="mdl-list__item-secondary-content">';
        bookingContent += '<a class="mdl-list__item-secondary-action" onclick="detailedView(' + bookingIndex + ')">';
        bookingContent += '<i class="material-icons">info</i></a>';
        bookingContent += '</span>';
        bookingContent += '</div>';
        // show booking info
        bookingContent += '<div class="mdl-grid mdl-card__supporting-text">';
        bookingContent += '<p class="mdl-cell mdl-cell--12-col align-text-center"><i class="material-icons">event</i>&nbsp;' + bookingList[bookingIndex].bookingDate + '</p>';
        bookingContent += '<p class="mdl-cell mdl-cell--3-col align-text-center"><i class="material-icons">person_pin_circle</i>&nbsp;' + bookingList[bookingIndex].pickUpPoint + '</p>';
        bookingContent += '<p class="mdl-cell mdl-cell--1-col align-text-center"><i class="material-icons">chevron_right</i></p>';
        bookingContent += '<p class="mdl-cell mdl-cell--3-col align-text-center"><i class="material-icons">place</i>&nbsp;' + bookingList[bookingIndex].destination + '</p>';
        bookingContent += '<p class="mdl-cell mdl-cell--12-col align-text-center"><i class="material-icons">multiple_stop</i>&nbsp;' + bookingList[bookingIndex].stops + 'Stop</p>';
        bookingContent += '<p class="mdl-cell mdl-cell--2-col align-text-center"><i class="material-icons">map</i>&nbsp;' + bookingList[bookingIndex].distance + 'Km</p>';
        bookingContent += '<p class="mdl-cell mdl-cell--2-col align-text-center"><i class="material-icons">paid</i>&nbsp;' + bookingList[bookingIndex].cost + '</p>';
        bookingContent += '<p class="mdl-cell mdl-cell--12-col align-text-center"><i class="material-icons">local_taxi</i>&nbsp;' + bookingList[bookingIndex].taxiType + '</p>';
        bookingContent += '</div>';
        bookingContent += '</div>';
    }
    document.getElementById("bookingContent").innerHTML = bookingContent;
}
// run when the page load
bookingList();


// show booking detail 
// ridirect the html page 
// return void
function detailedView(bookingIndex) {
  // update booking index to local storage
  updateLocalStorageData(BOOKING_INDEX_KEY, bookingIndex);
  // redirect to detailedBooking.html
  window.location = 'detailedBooking.html';
}

// the end of the code