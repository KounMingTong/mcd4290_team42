"use strict";
// Keys for localStorage
const BOOKING_INDEX_KEY = "bookingIndex";



// Booking Class
class Booking {

    // Booking class constructor
    // initialize class parameter
    constructor(bookingDate = null, pickUpPoint = null, destination = null, stops = null, distance = null, cost = null, taxiType = null) {
        this._bookingDate = bookingDate;
        this._pickUpPoint = pickUpPoint;
        this._destination = destination;
        this._stops = stops;
        this._distance = distance;
        this._cost = cost;
        this._taxiType = taxiType;
    }
    
    get bookingDate(){
        return this._bookingDate;
    }
    
    get pickUpPoint(){
        return this._pickUpPoint;
    }
    
    get destination(){
        return this._destination;
    }
    
    get cost(){
        return this._cost;
    }
    
    get distance(){
        return this._distance;
    }
    
    get stops(){
        return this._stops;
    }
    
    get taxiType(){
        return this._taxiType;
    }

    set bookingDate(newBookDate){
        this._bookingDate = newBookDate;
    }
    
    set pickUpPoint(newPickUpPoint){
        this._pickUpPoint = newPickUpPoint;
    }
    
    set destination(newDestination){
        this._destination = newDestination;
    }
    
    set cost(calcCost){
        this._cost = calcCost;
    }
    
    set distance(calcDist){
        this._distance = calcDist;
    }
    
    set stops(newStops){
        this._stops = newStops;
    }
    
    set taxiType(chooseType){
        this._taxiType = chooseType;
    }
    
    // initialize data from localstorage
    // to local attributes
    // return void
    fromData(data) {
        this._bookingDate = data.bookingDate;
        this._pickUpPoint = data.pickUpPoint;
        this._destination = data.destination;
        this._stops = data.stops;
        this._distance = data.distance;
        this._cost = data.cost;
        this._taxiType = data.taxiType;
    }

}

// check local storage data
// return boolean
function checkLocalStorageDataExist(key) {
    if (localStorage.getItem(key)) {
        return true;
    }
    return false;
}

// update data to local storage
// by key
// return void
function updateLocalStorageData(key, data) {
    let jsonData = JSON.stringify(data);
    localStorage.setItem(key, jsonData);
}

// get item from local storage
// by key
// return object 
function getLocalStorageData(key) {
    let jsonData = localStorage.getItem(key);
    let data = jsonData;

    try {
        data = JSON.parse(jsonData);
    } catch (e) {
        console.error(e);
    } finally {
        return data;
    }
}

