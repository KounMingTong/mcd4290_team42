/* 
* Purpose:  This file is to fulfill the MCD4290 Egineering Mobile Apps Assignment 2.
            The outcome of this file is a webpage that will run and allow the user
            to book a taxi within Victoria.
* Author:   Team 42 
            - Mateus Ariatama
            - Koun Ming Tong
            - Aliftarisdha M
* Last Modified: 12 Septemer 2021
*/  
"use strict";

//viewDetailedooking()
//This function will display the booking information including the date, pick up point
//destination, number of stops, distance, and the fare.
//The function does not have a parameter.
function viewDetailedBooking()
{
    //Display appropriate booking date
    document.getElementById('show-date').innerText = Booking.bookingDate;
    
    //Display pick up point and destination
    let output = '';
    output += '<p class="mdl-cell mdl-cell--3-col align-text-center">';
    output += '<i class="material-icons">place</i>&nbsp;' + Booking.pickUpPoint;
    output += '</p>';
    
    output += '<p class="mdl-cell mdl-cell--3-col align-text-center">';
    output += '<i class="material-icons">place</i>&nbsp;' + Booking.destination;
    output += '</p>';
    
    document.getElementById('show-routes').innerHTML = output;
    
    //Display number of stops
    let numOfStops = Booking.stops.length;
    document.getElementById('show-stop').innerText = numOfStops;
    
    //Display distance
    document.getElementById("show_distance").innerHTML = Booking.distance;
    
    //Display fare
    document.getElementById("show_fare").innerHTML = Booking.cost;
}

//changeTaxiType()
//This function allow the user to change the taxi type 
//The function does not have a parameter
function changeTaxitype()
{
    //Confirming the user
    if(confirm("Are you sure want to change the taxi type?"))
    {
        //Updating the taxi type of the booking if the user confirmed 
        newTaxiType = document.getElementById('change-type').value;
        Booking.setTaxiType(newTaxiType);
    }
    
}

//cancelBooking(bookingIndex)
//This function will cancel the booking if the user wish to
//The function takes one parameter which is the booking index
function cancelBooking(bookingIndex)
{
    //Confirming the user
    if(confirm("Are you sure want to cancel the booking?"))
    {
        //Removing the booking
        Booking.removeBooking(bookingIndex);
        updateLocalStorageData(BOOKING_INDEX_KEY, Booking); //Updating the booking data storage
    }
}

//bookingDone(bookingIndex)
//This function will mark the booking as done/completed
//The function take one parameter which is the booking Index
function bookingDone(bookingIndex)
{
    if(confirm("Mark this booking as done?"))
    {
        Booking.removeBooking(bookingIndex);
        updateLocalStorageData(BOOKING_INDEX_KEY, Booking); //Updating the booking data storage
    }
}

//Run the function when the page load
viewDetailedBooking();